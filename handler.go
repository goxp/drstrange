package drstrange

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
	"gitlab.com/goxp/cloud0/logger"
)

type JobHandler struct {
	cfg       *JobConfig
	id        int
	initOnce  sync.Once
	log       *logrus.Entry
	client    *http.Client
	prefix    string
	key       string
	redisConn *redis.Client
}

func NewJobHandler(cfg *JobConfig, prefix string, conn *redis.Client) *JobHandler {
	return &JobHandler{
		cfg:       cfg,
		prefix:    prefix,
		redisConn: conn,
	}
}

func (h *JobHandler) init() {
	h.log = logger.Tag("JobHandler").WithField("name", h.cfg.Name)

	if h.client == nil {
		h.client = &http.Client{
			// No follow redirect
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
	}

	if h.cfg.HandlerConfig.TimeoutSeconds == 0 {
		h.cfg.HandlerConfig.TimeoutSeconds = 10
	}

	// build key
	h.key = fmt.Sprintf("%s:%d", h.prefix, h.id)
}

func (h *JobHandler) shouldRun() bool {
	log := h.log.WithField("func", "shouldRun")
	pipeline := h.redisConn.Pipeline()
	ctx := context.Background()
	// read current value, set to lock next 30s if not exist
	read := pipeline.Get(ctx, h.key)
	_ = pipeline.SetNX(ctx, h.key, 1, time.Second*59)
	_, err := pipeline.Exec(ctx)
	if err != nil && err != redis.Nil {
		log.WithError(err).Error("failed to call redis")
		return false
	}

	if read.Val() != "" {
		return false
	}

	return true
}

func (h *JobHandler) Run() {
	h.initOnce.Do(h.init)

	cfg := h.cfg.HandlerConfig
	timestamp := time.Now()

	// check for running
	if !h.shouldRun() {
		h.log.Info("job is locked")
		return
	}

	h.log.WithField("time", timestamp.Format(time.RFC3339Nano)).Info("running job")
	timeout := time.Duration(cfg.TimeoutSeconds) * time.Second
	defer func() {
		duration := time.Since(timestamp)
		h.log.WithField("duration", duration.String()).Info("finish handling")
	}()

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, cfg.Method, cfg.URL, strings.NewReader(cfg.Body))
	if err != nil {
		h.log.WithError(err).Error("failed to create request")
		return
	}
	for k, v := range cfg.Headers {
		req.Header.Set(k, v)
	}
	req.Header.Set("x-time", fmt.Sprintf("%d", timestamp.Unix()))

	resp, err := h.client.Do(req)
	if err != nil {
		h.log.WithError(err).Error("failed to send request")
		return
	}

	// discard body to reuse tcp connection
	_, _ = io.Copy(io.Discard, resp.Body)
	_ = resp.Body.Close()
}
