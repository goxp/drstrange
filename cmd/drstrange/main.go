package main

import (
	"gitlab.com/goxp/drstrange"

	"gitlab.com/goxp/cloud0/logger"
)

func main() {
	err := drstrange.Run()
	if err != nil {
		logger.Tag("main").WithError(err).Error("failed to start service")
	}
}
