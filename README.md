# DrStrange

A distributed schedule job system.

## Dependencies

- `Redis`: it uses Redis as a global locking to avoid duplicate jobs.

## Get started

Check these environment to start service.

| Env         | Default           | Note            |
| ---         | -------           | --------------  |
| TIME_ZONE   | Asia/Ho_Chi_Minh  | timezone        |
| REDIS_ADDR  | localhost:6379    | redis host      |
| REDIS_PASS  | <empty>           | redis password  |
| REDIS_DB    | 0                 | redis DB

## Jobs configuration

To set up jobs, place perspective setting in `config.yml` file. 
Each item from root represents a job. Example

```yaml
- name: "testing-job-00"
  spec: "@every 10s"
  handler:
    method: POST
    url: https://aingaeheechub9ae.free.beeceptor.com
    headers:
      content-type: application/json
    body: |
      {"name": "testing"}
```

