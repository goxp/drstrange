module gitlab.com/goxp/drstrange

go 1.16

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gin-gonic/gin v1.7.2
	github.com/go-redis/redis/v8 v8.11.3
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/goxp/cloud0 v1.4.5
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
