package drstrange

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/robfig/cron/v3"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/goxp/cloud0/service"
	"gopkg.in/yaml.v3"
)

type App struct {
	*service.BaseApp
	*Setting

	configJobs  []*JobConfig
	jobEngine   *cron.Cron
	redisClient *redis.Client
}

var (
	version = "v1.0.0"
	name    = "drstrange"
)

func NewApp() *App {
	return &App{
		BaseApp: service.NewApp(name, version),
		Setting: &Setting{},
	}
}

// Run initializes service & start serving
func Run() error {
	gin.SetMode(gin.ReleaseMode)
	logger.Init(name)
	app := NewApp()
	err := app.init()
	if err != nil {
		return err
	}

	app.jobEngine.Start()

	return app.Start(context.Background())
}

func (a *App) init() error {
	err := a.Initialize()
	if err != nil {
		return err
	}

	err = env.Parse(a.Setting)
	if err != nil {
		return errors.New("error while parsing extra setting: " + err.Error())
	}

	a.configJobs, err = a.loadConfiguredJobs()
	if err != nil {
		return fmt.Errorf("load jobs error: %v", err)
	}

	// init job engine
	loc, err := time.LoadLocation(a.TimeZone)
	if err != nil {
		return fmt.Errorf("failed to parse timezone: %v", err)
	}
	a.jobEngine = cron.New(cron.WithLocation(loc))

	a.redisClient = redis.NewClient(&redis.Options{
		Addr:     a.RedisAddr,
		Password: a.RedisPass,
		DB:       a.RedisDB,
	})

	if err := a.redisClient.Ping(context.Background()).Err(); err != nil {
		return fmt.Errorf("failed to connect redis: %v", err)
	}

	if err = a.registerJobs(); err != nil {
		return fmt.Errorf("failed to register jobs: %v", err)
	}

	return nil
}

// loadConfiguredJobs loads jobs from yaml file
func (a *App) loadConfiguredJobs() ([]*JobConfig, error) {
	raw, err := ioutil.ReadFile(a.ConfigFile)
	if err != nil {
		return nil, fmt.Errorf("failed read config file: %v", err)
	}

	var jobs []*JobConfig
	err = yaml.Unmarshal(raw, &jobs)
	if err != nil {
		return nil, fmt.Errorf("failed to decode config data: %v", err)
	}

	return jobs, nil
}

// registerJobs registers job to cron engine
func (a *App) registerJobs() error {
	log := logger.Tag("App.registerJobs")

	for i, jobCfg := range a.configJobs {
		// fill a name if empty
		if jobCfg.Name == "" {
			jobCfg.Name = fmt.Sprintf("job-%d", i)
		}

		handler := NewJobHandler(jobCfg, name+":"+version+":"+a.Config.Env, a.redisClient)
		jobID, err := a.jobEngine.AddJob(jobCfg.Spec, handler)
		if err != nil {
			return fmt.Errorf("register job error (name: %s): %v", jobCfg.Name, err)
		}
		handler.id = int(jobID)

		log.WithField("name", jobCfg.Name).WithField("spec", jobCfg.Spec).Info("register job")
	}

	return nil
}
