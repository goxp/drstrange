package drstrange

// Setting represents app extra setting
type Setting struct {
	ConfigFile string `env:"CONFIG_FILE" envDefault:"config.yml"`
	TimeZone   string `env:"TIME_ZONE" envDefault:"Asia/Ho_Chi_Minh"`
	RedisAddr  string `env:"REDIS_ADDR" envDefault:"localhost:6379"`
	RedisPass  string `env:"REDIS_PASS" envDefault:""`
	RedisDB    int    `env:"REDIS_DB" envDefault:"0"`
}

// JobConfig represents a job configuration at a time
type JobConfig struct {
	Name          string       `yaml:"name"`
	Spec          string       `yaml:"spec"`
	HandlerConfig *HandlerHttp `yaml:"handler"`
}

// HandlerHttp represents a http handler configuration
type HandlerHttp struct {
	Method         string            `yaml:"method"`
	URL            string            `yaml:"url"`
	Headers        map[string]string `yaml:"headers"`
	Body           string            `yaml:"body"`
	TimeoutSeconds int               `yaml:"timeout_seconds"`
}
